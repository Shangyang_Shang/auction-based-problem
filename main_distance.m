clear;

N = 500;
N_G = 2;
N_E = (N/N_G)*ones(2,1);
bd_s = 40;
R = 10;
% d = 2:1:13;
% d = 14:1:20;
d = 10:1:20;

N_D = 8;
a = 0.1;
small_value = -100;
gamma_table_dB = [-6 -9 -12 -15 -17.5 -20];
gamma_table = 10.^(gamma_table_dB./10);
% Tij_table = [0.036 0.064 0.113 0.204 0.365 0.682];
Tij_table = [0.34842 0.61491 0.61541 0.61645 1.31482 2.46579];
T = 0.65;
N_i = 100; % Number of iteration (in auction algorithm)
epsilon = 1;
N_r = 200; % Number of random
beta = 0.001;

obj_d = zeros(length(d), 1);
obj_g_d = zeros(length(d), 1);
obj_bm1_d = zeros(length(d), 1);
obj_bm2_d = zeros(length(d), 1);
obj_bm3_d = zeros(length(d), 1);
ic_d = zeros(length(d),1);
ic_g_d = zeros(length(d),1);
for i = 1:length(d)
    S = [10 10;10+d(i) 10];
    obj = 0;
    obj_g = 0;
    obj_bm1 = 0;
    obj_bm2 = 0;
    obj_bm3 = 0;
    ic = 0;
    ic_g = 0;
    for j = 1:N_r
        % Coordinate of end-devices
        s = coordinate_E(N_E, N_G, S, R);
        % Find parameters
        [r1j, r2j] = find_distance(N, s, S);
        J_overlap = find_overlap(r1j, r2j, R, N);
        [cp, SF, Tij] = find_capture_prob(r1j, r2j, R, N, Tij_table, gamma_table);
        % Initial: xij = 0 for overlap area
        X = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
        X(J_overlap,:) = zeros(length(J_overlap),2);
        th1_tmp = N_D*T - a*sum(Tij.*X(:,1).*cp(:,1));
        th2_tmp = N_D*T - a*sum(Tij.*X(:,2).*cp(:,2));
        th1 = max(th1_tmp, 0);
        th2 = max(th2_tmp, 0);
        % Grouping
        [g1, g2, g3, g4, g5, g6] = grouping(J_overlap, SF);
        [p1, p2, p3, p4, p5, p6] = find_proportion(g1, g2, g3, g4, g5, g6);
        % Auction algorithm (Output is an array)
        [obj_tmp, ic_tmp, X_ng_tmp] = auction_algorithm(N_i, epsilon, J_overlap, N, a, cp, small_value, Tij, th1, th2, beta);
        [obj_tmp1, ic_tmp1, X_g1_tmp] = auction_algorithm(N_i, epsilon, g1, N, a, cp, small_value, Tij, p1*th1, p1*th2, beta);
        [obj_tmp2, ic_tmp2, X_g2_tmp] = auction_algorithm(N_i, epsilon, g2, N, a, cp, small_value, Tij, p2*th1, p2*th2, beta);
        [obj_tmp3, ic_tmp3, X_g3_tmp] = auction_algorithm(N_i, epsilon, g3, N, a, cp, small_value, Tij, p3*th1, p3*th2, beta);
        [obj_tmp4, ic_tmp4, X_g4_tmp] = auction_algorithm(N_i, epsilon, g4, N, a, cp, small_value, Tij, p4*th1, p4*th2, beta);
        [obj_tmp5, ic_tmp5, X_g5_tmp] = auction_algorithm(N_i, epsilon, g5, N, a, cp, small_value, Tij, p5*th1, p5*th2, beta);
        [obj_tmp6, ic_tmp6, X_g6_tmp] = auction_algorithm(N_i, epsilon, g6, N, a, cp, small_value, Tij, p6*th1, p6*th2, beta);
        % Find benchmarks (Output is a number)
        [obj_bm1_tmp, X_bm1] = benchmark_1(cp, J_overlap, Tij, N_D, T, N, a);
        [obj_bm2_tmp, X_bm2] = benchmark_2(cp, J_overlap, Tij, N_D, T, N, a, N_i, beta);
        [obj_bm3_tmp, X_ub] = benchmark_3(N, J_overlap, cp, a);
        % Sum metrics
        X = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
        X(J_overlap,:) = zeros(length(J_overlap),2);
        obj = obj + obj_tmp + a*sum(sum(X.*cp));
        obj_g = obj_g + obj_tmp1 + obj_tmp2 + obj_tmp3 + obj_tmp4 + obj_tmp5 + obj_tmp6 + a*sum(sum(X.*cp));
        obj_bm1 = obj_bm1 + obj_bm1_tmp;
        obj_bm2 = obj_bm2 + obj_bm2_tmp;
        obj_bm3 = obj_bm3 + obj_bm3_tmp;
        ic_g_tmp = max([ic_tmp1 ic_tmp2 ic_tmp3 ic_tmp4 ic_tmp5 ic_tmp6]);
        ic = ic + ic_tmp;
        ic_g = ic_g + ic_g_tmp;
        % Save output
        X_ng = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
        X_ng(J_overlap,:) = X_ng_tmp(J_overlap,:);
        X_g = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
        X_g(g1,:) = X_g1_tmp(g1,:);
        X_g(g2,:) = X_g2_tmp(g2,:);
        X_g(g3,:) = X_g3_tmp(g3,:);
        X_g(g4,:) = X_g4_tmp(g4,:);
        X_g(g5,:) = X_g5_tmp(g5,:);
        X_g(g6,:) = X_g6_tmp(g6,:);
%         str = append('D:/Distance_GW2/data_var_pow/data_d_',num2str(d(i)),'_num_',num2str(j),'.mat');
%         save(str, 'cp', 'SF', 'X_g', 'X_ng','X_bm1','X_bm2', 'X_ub', 's', 'S', 'ic_tmp', 'ic_g_tmp')
    end
    obj_d(i) = obj/N_r;
    obj_g_d(i) = obj_g/N_r;
    obj_bm1_d(i) = obj_bm1/N_r;
    obj_bm2_d(i) = obj_bm2/N_r;
    obj_bm3_d(i) = obj_bm3/N_r;
    ic_d(i) = ic/N_r;
    ic_g_d(i) = ic_g/N_r;
end

% disp(N_D*T)
% disp([a*sum(X_ng(:,1).*cp(:,1).*Tij) a*sum(X_ng(:,2).*cp(:,2).*Tij)])
% disp([a*sum(X_g(:,1).*cp(:,1).*Tij) a*sum(X_g(:,2).*cp(:,2).*Tij)])
% disp([obj_d obj_g_d])
% disp([sum(sum(X_ng)) sum(sum(X_g))])

% Plot objective function
figure(1)
plot(d, obj_bm3_d, '-or', 'LineWidth', 1.5); hold on
plot(d, obj_bm1_d, '--or', 'LineWidth', 1.5); hold on
plot(d, obj_bm2_d, '-.or', 'LineWidth', 1.5); hold on
plot(d, obj_d, '--ob', 'LineWidth', 1.5); hold on
plot(d, obj_g_d, '-ob', 'LineWidth', 1.5); hold off
% axis([0 13 min([obj_d(1) obj_g_d(1) obj_bm1_d(1) obj_bm2_d(1) obj_bm3_d(1)]) max([obj_d(12) obj_g_d(12) obj_bm1_d(12) obj_bm2_d(12) obj_bm3_d(12)])])
grid on
title('Objective function')
xlabel('Distance')
legend('Upper bound', 'Benchmark 1', 'Benchmark 2', 'Non-grouping', 'Grouping')
% Plot iteration of convergence
figure(2)
plot(d, ic_d, '--ob', 'LineWidth', 1.5); hold on
plot(d, ic_g_d, '-ob', 'LineWidth', 1.5); hold off
grid on
title('Objective function')
xlabel('Distance')
legend('Non-grouping', 'Grouping')

