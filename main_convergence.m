clear;

N = 500;
N_G = 2;
N_E = (N/N_G)*ones(2,1);
bd_s = 40;
R = 10;
d = 16;


N_D = 8;
a = 0.1;
small_value = -100;
gamma_table_dB = [-6 -9 -12 -15 -17.5 -20];
gamma_table = 10.^(gamma_table_dB./10);
% Tij_table = [0.036 0.064 0.113 0.204 0.365 0.682];
Tij_table = [0.34842 0.61491 0.61541 0.61645 1.31482 2.46579];
T = 0.7;
N_i = 200; % Number of iteration (in auction algorithm)
epsilon = 1;
N_ba = 100; % Number of benchmark average


S = [10 10;10+d 10];
obj = zeros(N_i, 1);
obj_g = zeros(N_i, 1);
obj_bm1 = 0;
obj_bm2 = 0;
obj_bm3 = 0;
ic = 0;
ic_g = 0;
n_overlap = 0;
%ng = zeros(6, 1); % Number of end-devices in each group
N_r = 1; % Number of random
for i = 1:N_r
    % Coordinate of end-devices
    s = coordinate_E(N_E, N_G, S, R);
    % Find parameters
    [r1j, r2j] = find_distance(N, s, S);
    J_overlap = find_overlap(r1j, r2j, R, N);
    [cp, SF, Tij] = find_capture_prob(r1j, r2j, R, N, Tij_table, gamma_table);
    % Initial: xij = 0 for overlap area
    X = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
    X(J_overlap,:) = zeros(length(J_overlap),2);
    th1 = N_D*T - a*sum(Tij.*X(:,1).*cp(:,1));
    th2 = N_D*T - a*sum(Tij.*X(:,2).*cp(:,2));
    % Grouping
    [g1, g2, g3, g4, g5, g6] = grouping(J_overlap, SF);
    [p1, p2, p3, p4, p5, p6] = find_proportion(g1, g2, g3, g4, g5, g6);
    % Auction algorithm (Output is an array)
    [obj_tmp, ic_tmp] = auction_algorithm(N_i, epsilon, J_overlap, N, a, cp, small_value, Tij, th1, th2);
    [obj_tmp1, ic_tmp1] = auction_algorithm(N_i, epsilon, g1, N, a, cp, small_value, Tij, p1*th1, p1*th2);
    [obj_tmp2, ic_tmp2] = auction_algorithm(N_i, epsilon, g2, N, a, cp, small_value, Tij, p2*th1, p2*th2);
    [obj_tmp3, ic_tmp3] = auction_algorithm(N_i, epsilon, g3, N, a, cp, small_value, Tij, p3*th1, p3*th2);
    [obj_tmp4, ic_tmp4] = auction_algorithm(N_i, epsilon, g4, N, a, cp, small_value, Tij, p4*th1, p4*th2);
    [obj_tmp5, ic_tmp5] = auction_algorithm(N_i, epsilon, g5, N, a, cp, small_value, Tij, p5*th1, p5*th2);
    [obj_tmp6, ic_tmp6] = auction_algorithm(N_i, epsilon, g6, N, a, cp, small_value, Tij, p6*th1, p6*th2);
    % Find benchmarks (Output is a number)
    obj_bm1_tmp = benchmark_1(N_ba, cp, J_overlap, Tij, N_D, T, N, a);
    obj_bm2_tmp = benchmark_2(N_ba, cp, J_overlap, Tij, N_D, T, N, a);
    obj_bm3_tmp = benchmark_3(N, J_overlap, cp, a);
    % Sum metrics
    n_overlap = n_overlap + length(J_overlap);
    obj = obj + obj_tmp + a*sum(sum(X.*cp));
    obj_g = obj_g + obj_tmp1 + obj_tmp2 + obj_tmp3 + obj_tmp4 + obj_tmp5 + obj_tmp6 + a*sum(sum(X.*cp));
    obj_bm1 = obj_bm1 + obj_bm1_tmp;
    obj_bm2 = obj_bm2 + obj_bm2_tmp;
    obj_bm3 = obj_bm3 + obj_bm3_tmp;
    ic = ic + ic_tmp;
    ic_g = ic_g + max([ic_tmp1 ic_tmp2 ic_tmp3 ic_tmp4 ic_tmp5 ic_tmp6]);
    %ng = ng + [length(g1);length(g2);length(g3);length(g4);length(g5);length(g6)];
end
n_overlap = n_overlap/N_r;
obj_d = obj/N_r;
obj_g_d = obj_g/N_r;
obj_bm1_d = obj_bm1/N_r;
obj_bm2_d = obj_bm2/N_r;
obj_bm3_d = obj_bm3/N_r;
ic = ic/N_r;
ic_g = ic_g/N_r;

% Plot objective function
figure(1)
plot(1:N_i, obj_bm3_d*ones(N_i,1), '-r', 'LineWidth', 1.5); hold on
plot(1:N_i, obj_bm1_d*ones(N_i,1), '--r', 'LineWidth', 1.5); hold on
plot(1:N_i, obj_bm2_d*ones(N_i,1), '-.r', 'LineWidth', 1.5); hold on
plot(1:N_i, obj_d, '--b', 'LineWidth', 1.5); hold on
plot(1:N_i, obj_g_d, '-b', 'LineWidth', 1.5); hold off
grid on
title('Objective function')
xlabel('Distance')
legend('Upper bound', 'Benchmark 1', 'Benchmark 2', 'Non-grouping', 'Grouping')



% % Plot objective function
% figure(1)
% plot(1:N_i, obj_bm3*ones(N_i,1), '-r', 'LineWidth', 1.5); hold on
% plot(1:N_i, obj_bm1*ones(N_i,1), '--r', 'LineWidth', 1.5); hold on
% plot(1:N_i, obj_bm2*ones(N_i,1), '-.r', 'LineWidth', 1.5); hold on
% plot(1:N_i, obj, '-b', 'LineWidth', 1.5); hold off
% grid on
% title('Objective function')
% xlabel('Number of iteration')
% ylabel('Objective function')
% legend('Upper bound', 'Benchmark 1', 'Benchmark 2', 'Objective function')
% % Plot total number of assignmants
% figure(3)
% plot(1:N_i, N*ones(N_i,1), '-r', 'LineWidth', 1.5); hold on
% plot(1:N_i, na, '-b', 'LineWidth', 1.5); hold off
% grid on
% title('Total number of assignments')
% xlabel('Number of iteration')
% ylabel('Total number of assignments')
% legend('Upper bound', 'Number of assignments')





