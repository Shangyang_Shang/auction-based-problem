% Availiable time - Utility function
% Availiable time - Execution time
clear;
% Parameter
d = 1.4;
N = 500;
R = 1;
N_D = 8;
a = 1;
small_value = -100;
alpha = 3;
gamma_table_dB = [-6 -9 -12 -15 -17.5 -20];
gamma_table = 10.^(gamma_table_dB./10);
Tij_table = [0.34842 0.61491 0.61541 0.61645 1.31482 2.46579];
N_i = 100; % Number of iteration (in auction algorithm)
epsilon = 1;
Ns = 1e3; % Number of samples
beta = 0.0001;
bit_rate_table = [5.47 3.13 1.76 0.98 0.54 0.29]';
% Control
avail_time = 0:0.1:3;
% avail_time = 0:0.02:0.2;
% Initialization
obj      = zeros(1, length(avail_time));
obj_g    = zeros(1, length(avail_time));
obj_bm1  = zeros(1, length(avail_time));
obj_bm2  = zeros(1, length(avail_time));
obj_ub   = zeros(1, length(avail_time));
ic_avg   = zeros(1, length(avail_time));
ic_g_avg = zeros(1, length(avail_time));
t_auc = zeros(1, length(avail_time));
t_grp = zeros(1, length(avail_time));
t_cnv = zeros(1, length(avail_time));
t_auc_avg = zeros(1, length(avail_time));
t_grp_avg = zeros(1, length(avail_time));
t_cnv_avg = zeros(1, length(avail_time));
num_bm1 = zeros(1, length(avail_time));
num_bm2 = zeros(1, length(avail_time));
num_ng = zeros(1, length(avail_time));
num_g = zeros(1, length(avail_time));
load('D:/opt_prob_data/fix_pow_near/d_14.mat');
% t_idx: index of available time
% s_idx: index of sample
for t_idx = 1:length(avail_time)
    disp(['available time: ' num2str(avail_time(t_idx))])
    for s_idx = 1:Ns
        str = append('D:/opt_prob_data/fix_pow_near/d_14/', num2str(s_idx));
%         str = append('D:/opt_prob_data/fix_pow_near_original/d_14_num_', num2str(s_idx));
        load(str)
%         s_sel = s;
%         cp_sel = cp;
%         SF_sel = SF;
        % Find parameters
        [r1j, r2j] = find_distance(N, s_sel, S);
%         [cp_sel, SF_sel, ~] = find_capture_prob(r1j, r2j, d+R, N, Tij_table, gamma_table, alpha);
        J_overlap = find_overlap(r1j, r2j, R, N, cp_sel);
        Tij = Tij_table(SF_sel-6)';
        % Initial: xij = 0 for overlap area
        X = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
        X(J_overlap,:) = zeros(length(J_overlap),2);
        th1_tmp = N_D*avail_time(t_idx) - a*sum(Tij.*X(:,1).*cp_sel(:,1));
        th2_tmp = N_D*avail_time(t_idx) - a*sum(Tij.*X(:,2).*cp_sel(:,2));
        th1 = max(th1_tmp, 0);
        th2 = max(th2_tmp, 0);
        % Grouping
        g1 = J_overlap(1:2:end);
        g2 = J_overlap(2:2:end);
        % Auction algorithm (Output is an array)
        tic
        [obj_tmp, ic_tmp, X_ng_tmp] = auction_algorithm(N_i, epsilon, J_overlap, N, a, cp_sel, small_value, Tij, th1, th2, beta);
        t_auc(t_idx) = t_auc(t_idx) + toc;
        if ~isempty(ic_tmp)
            t_auc_avg(t_idx) = t_auc_avg(t_idx) + toc/ic_tmp;
        end
        tic
        [obj_tmp1, ic_tmp1, X_g1_tmp] = auction_algorithm(N_i, epsilon, g1, N, a, cp_sel, small_value, Tij, 0.5*th1, 0.5*th2, beta);
        t_grp_1 = toc;
        tic
        [obj_tmp2, ic_tmp2, X_g2_tmp] = auction_algorithm(N_i, epsilon, g2, N, a, cp_sel, small_value, Tij, 0.5*th1, 0.5*th2, beta);
        t_grp_2 = toc;
        t_grp(t_idx) = t_grp(t_idx) + max([t_grp_1 t_grp_2]);
        if ~isempty(ic_tmp1) && ~isempty(ic_tmp2)
            t_grp_avg(t_idx) = t_grp_avg(t_idx) + max([t_grp_1/ic_tmp1 t_grp_2/ic_tmp2]);
        end
        % Find benchmarks (Output is a number)
        ran_ed = zeros(1, length(J_overlap));
        ran_gw = zeros(1,length(J_overlap));
        for k = 1:length(J_overlap)
            ran_ed(k) = randi([1 length(J_overlap)-k+1]);
            ran_gw(k) = randi([1 2]);
        end
        [obj_bm1_tmp, X_bm1] = benchmark_1(cp_sel, J_overlap, Tij, N_D, avail_time(t_idx), N, a, ran_ed);
        tic
        [obj_bm2_tmp, X_bm2, n_bm2] = benchmark_2(cp_sel, J_overlap, Tij, N_D, avail_time(t_idx), N, a, ran_ed, ran_gw);
        t_cnv(t_idx) = t_cnv(t_idx) + toc;
        if ~isempty(n_bm2)
            t_cnv_avg(t_idx) = t_cnv_avg(t_idx) + toc/n_bm2;
        end
        [obj_bm3_tmp, X_ub] = benchmark_3(N, J_overlap, cp_sel, a);
        % Save output
        X_ng = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
        X_ng(J_overlap,:) = X_ng_tmp(J_overlap,:);
        X_g = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
        X_g(g1,:) = X_g1_tmp(g1,:);
        X_g(g2,:) = X_g2_tmp(g2,:);
        num_bm1(t_idx) = num_bm1(t_idx) + sum(sum(X_bm1));
        num_bm2(t_idx) = num_bm2(t_idx) + sum(sum(X_bm2));
        num_ng(t_idx) = num_ng(t_idx) + sum(sum(X_ng));
        num_g(t_idx) = num_g(t_idx) + sum(sum(X_g));

        obj(t_idx)     = obj(t_idx) + a*sum(sum(X_ng.*cp_sel));
        obj_g(t_idx)   = obj_g(t_idx) + a*sum(sum(X_g.*cp_sel));
        obj_bm1(t_idx) = obj_bm1(t_idx) + a*sum(sum(X_bm1.*cp_sel));
        obj_bm2(t_idx) = obj_bm2(t_idx) + a*sum(sum(X_bm2.*cp_sel));
        obj_ub(t_idx)  = obj_ub(t_idx) + a*sum(sum(X_ub.*cp_sel));
        
        ic_avg(t_idx) = ic_avg(t_idx) + ic_tmp;
        ic_g_avg(t_idx) = ic_g_avg(t_idx) + max([ic_tmp1 ic_tmp2]);
    end
end
obj = obj/Ns;
obj_g = obj_g/Ns;
obj_bm1 = obj_bm1/Ns;
obj_bm2 = obj_bm2/Ns;
obj_ub = obj_ub/Ns;

t_auc = t_auc/Ns;
t_grp = t_grp/Ns;
t_cnv = t_cnv/Ns;

t_auc_avg = t_auc_avg/Ns;
t_grp_avg = t_grp_avg/Ns;
t_cnv_avg = t_cnv_avg/Ns;

ic_avg = ic_avg/Ns;
ic_g_avg = ic_g_avg/Ns;
% disp([sum(sum(X_bm1)) sum(sum(X_bm2)) sum(sum(X_ng)) sum(sum(X_g))])
% disp([obj_ub obj_bm1/sum(sum(X_bm1))*sum(sum(X_ng)) obj_bm2/sum(sum(X_bm2))*sum(sum(X_ng)) obj obj_g/sum(sum(X_g))*sum(sum(X_ng))])

% Plot objective function
figure(1)
% plot(avail_time, obj_ub, '-^r', 'LineWidth', 1.5); hold on
% plot(avail_time, obj_bm1, '-*k', 'LineWidth', 1.5); hold on
% plot(avail_time, obj_bm2, '--*k', 'LineWidth', 1.5); hold on
% plot(avail_time, obj, '-ob', 'LineWidth', 1.5); hold on
% plot(avail_time, obj_g, '--ob', 'LineWidth', 1.5); hold off
plot(avail_time, obj_ub, '-^r', 'LineWidth', 1.5); hold on
plot(avail_time, obj_bm1./sum(sum(num_bm1)).*sum(sum(num_ng)), '-*k', 'LineWidth', 1.5); hold on
plot(avail_time, obj_bm2./sum(sum(num_bm2)).*sum(sum(num_ng)), '--*k', 'LineWidth', 1.5); hold on
plot(avail_time, obj, '-ob', 'LineWidth', 1.5); hold on
plot(avail_time, obj_g./sum(sum(num_g)).*sum(sum(num_ng)), '--ob', 'LineWidth', 1.5); hold off
grid on
xlabel('Available time (sec)')
% ylabel('Utility function')
ylabel('Normalized utility function')
legend('Upper bound', 'Conv.-NS', 'Conv.-RS', 'Prop. Auction Alg.', 'Prop. Grouped Alg.')

% Plot time complexity
figure(2)
subplot(211)
plot(avail_time, t_cnv*1e3, '--*k', 'LineWidth', 1.5); hold on
plot(avail_time, t_auc*1e3, '-ob', 'LineWidth', 1.5); hold on
plot(avail_time, t_grp*1e3, '--ob', 'LineWidth', 1.5); hold off
% axis([0 3 0 1.6])
grid on
title('(a) Total average execution time')
xlabel('Available time (sec)')
ylabel('Execution time (ms)')
legend('Conv.-RS', 'Prop. Auction Alg.', 'Prop. Grouped Alg.')
subplot(212)
plot(avail_time, t_cnv_avg*1e3, '--*k', 'LineWidth', 1.5); hold on
plot(avail_time, t_auc_avg*1e3, '-ob', 'LineWidth', 1.5); hold on
plot(avail_time, t_grp_avg*1e3, '--ob', 'LineWidth', 1.5); hold off
% axis([0 3 0 0.015])
grid on
title('(b) Execution time per iteration')
xlabel('Available time (sec)')
ylabel('Execution time (ms)')
legend('Conv.-RS', 'Prop. Auction Alg.', 'Prop. Grouped Alg.')
