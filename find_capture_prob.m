function [cp, SF, Tij] = find_capture_prob(r1, r2, R, N, Tij_table, gamma_table, alpha)
%     SF = zeros(N,1);
%     Pt_dBm = zeros(N,1);
%     for i = 1:N
%         SF(i) = find_SF(r1(i), r2(i), R+4);
%         if SF(i)==12
%             Pt_dBm(i) = 20;
%         else
%             Pt_dBm(i) = 2+(SF(i)-7)*3;
%         end
%     end
    SF = zeros(N,1);
    for i = 1:N
        SF(i) = find_SF(r1(i), r2(i), R);
    end
    Pt_dBm = 14*ones(N,1);

    Pt = 10.^(Pt_dBm./10).*0.001;
    Tij = Tij_table(SF-6)';

    col_t = zeros(N,N);
    % i: desired signal
    % j: inteference
    for i = 1:N
        for j = 1:N
            col_t(i,j) = col_time(Tij(i), Tij(j), SF, Tij_table, N);
        end
    end
    gamma = zeros(N,1);
    for i = 1:N
        gamma(i) = gamma_table(SF(i)-6);
    end
    cp = zeros(N,2);
    for i = 1:N
        cp(i,1) = capture_prob(r1, Pt, gamma(i), col_t(i,:)', i, alpha);
        cp(i,2) = capture_prob(r2, Pt, gamma(i), col_t(i,:)', i, alpha);
    end
end