clear;
N = 500;
N_G = 2;
N_E = (N/N_G)*ones(2,1);
R = 1;
% d = 1:0.2:2;
d = 1.4;
alpha = 3;

gamma_table_dB = [-6 -9 -12 -15 -17.5 -20];
gamma_table = 10.^(gamma_table_dB./10);
Tij_table = [0.34842 0.61491 0.61541 0.61645 1.31482 2.46579];
N_r = 100; % Number of random (sample)

for i = 1:length(d)
    S = [10 10;10+d(i) 10];
    for j = 1:N_r
        disp(['d = ' num2str(d(i)) ', n = ' num2str(j)])
        % Coordinate of end-devices
        s = coordinate_E(N_E, N_G, S, R);
        % Find parameters
        [r1, r2] = find_distance(N, s, S);
        [cp, SF, Tij] = find_capture_prob(r1, r2, d(i)+R, N, Tij_table, gamma_table, alpha);
        str = append('D:/opt_prob_data/fix_pow_near_original/d_',num2str(d(i)*10),'_num_',num2str(j),'.mat');
        save(str, 'cp', 'SF', 's', 'S')
    end
end

% % 930 sec for one distance
% for i = 1:length(d)
%     tic
%     disp(['d = ' num2str(d(i))])
%     % Position of gateways
%     S = [10 10;10+d(i) 10];
%     % Position of end-devices
%     s = coordinate_E(N_E, N_G, S, R);
%     % Find parameters
%     [r1, r2] = find_distance(N, s, S);
%     [cp, SF, Tij] = find_capture_prob(r1, r2, d(i)+R, N, Tij_table, gamma_table, alpha);
%     str = append('D:/opt_prob_data/fix_pow_near/d_',num2str(d(i)*10),'.mat');
%     save(str, 'cp', 'SF', 's', 'S')
%     toc
% end