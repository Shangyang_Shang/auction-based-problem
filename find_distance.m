function [r1, r2] = find_distance(N, s, S)
    % r1: Distance between gateway 1
    % r2: Distance between gateway 2
    r1 = zeros(N,1);
    r2 = zeros(N,1);
    for i = 1:N
        r1(i) = sqrt((s(i,1)-S(1,1))^2 + (s(i,2)-S(1,2))^2);
        r2(i) = sqrt((s(i,1)-S(2,1))^2 + (s(i,2)-S(2,2))^2);
    end
end