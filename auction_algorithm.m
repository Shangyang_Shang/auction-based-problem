function [obj, ic, X] = auction_algorithm(N_i, epsilon, J_overlap, N, a, cp, small_value, Tij, th1, th2, beta)
    % Auction algorithm for non-grouping
    p = zeros(length(J_overlap),1);
    X = zeros(N,2);
    obj = zeros(1, N_i);
    for i = 1:N_i
        if isempty(J_overlap)
            obj = 0;
            ic = 0;
            break;
        end
        if length(J_overlap)==1
            j1 = 1;
            j2 = 1;
            B1j = cp(1,1) + epsilon;
            B2j = cp(1,2) + epsilon;
        else
            % Gateway 1
            v_1j = cp(J_overlap,1) - p(:);
            [v_1j1, j1] = max(v_1j);
            v_1j(j1) = small_value;
            v_1j2 = max(v_1j);
            B1j = p(j1) + v_1j1 - v_1j2 + epsilon;
            % Gateway 2
            v_2j = cp(J_overlap,2) - p(:);
            [v_2j1, j2] = max(v_2j);
            v_2j(j2) = small_value;
            v_2j2 = max(v_2j);
            B2j = p(j2) + v_2j1 - v_2j2 + epsilon;
        end
        % Center controller
        if J_overlap(j1)==J_overlap(j2)
            if B1j>B2j && a*Tij(J_overlap(j1))*cp(J_overlap(j1),1)+a*sum(Tij.*X(:,1).*cp(:,1))<=th1
                p(j1) = B1j;
                X(J_overlap(j1),:) = [1 0];
            end
            if B1j<=B2j && a*Tij(J_overlap(j2))*cp(J_overlap(j2),2)+a*sum(Tij.*X(:,2).*cp(:,2))<=th2
                p(j2) = B2j;
                X(J_overlap(j2),:) = [0 1];
            end
        else
            if a*Tij(J_overlap(j1))*cp(J_overlap(j1),1)+a*sum(Tij.*X(:,1).*cp(:,1))<=th1
                p(j1) = B1j;
                X(J_overlap(j1),:) = [1 0];
            end
            if a*Tij(J_overlap(j2))*cp(J_overlap(j2),2)+a*sum(Tij.*X(:,2).*cp(:,2))<=th2
                p(j2) = B2j;
                X(J_overlap(j2),:) = [0 1];
            end
        end
        obj(i) = a*sum(sum(X.*cp));
        if (i>1 && abs(obj(i)-obj(i-1))<=beta*obj(i-1)) || i==N_i
            ic = i; % Iteration of convergence
            obj = obj(ic);
            break;
        end
    end
end