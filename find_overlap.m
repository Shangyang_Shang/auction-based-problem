function J_overlap = find_overlap(r1j, r2j, R, N, cp)
    % Find J_overlap/J_nonoverlap
    J_overlap = zeros(N,1);
    j_o = 0;
    for i = 1:N/2
%         if r2j(i)<R && (cp(i,1)>0.0001 || cp(i,2)>0.0001)
        if cp(i,1)>0.00001 || cp(i,2)>0.00001
            j_o = j_o + 1;
            J_overlap(j_o) = i;
        end
    end
    for i = (N/2+1):N
%         if r1j(i)<R && (cp(i,1)>0.0001 || cp(i,2)>0.0001)
        if cp(i,1)>0.00001 || cp(i,2)>0.00001
            j_o = j_o + 1;
            J_overlap(j_o) = i;
        end
    end
    J_overlap = J_overlap(1:j_o);
end