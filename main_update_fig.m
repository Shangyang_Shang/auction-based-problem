clear;
% Parameter
d = 1:0.1:2;
N = 500;
R = 10;
N_D = 8;
a = 1;
small_value = -100;
Tij_table = [0.34842 0.61491 0.61541 0.61645 1.31482 2.46579];
T = 0.2;
N_i = 1000; % Number of iteration (in auction algorithm)
epsilon = 1;
N_r = 1000; % Number of random
beta = 0.0001;
bit_rate_table = [5.47 3.13 1.76 0.98 0.54 0.29]';
% Initialization
obj     = zeros(1, length(d));
obj_g   = zeros(1, length(d));
obj_bm1 = zeros(1, length(d));
obj_bm2 = zeros(1, length(d));
obj_ub  = zeros(1, length(d));
sum_rate     = zeros(1, length(d));
sum_rate_g   = zeros(1, length(d));
sum_rate_bm1 = zeros(1, length(d));
sum_rate_bm2 = zeros(1, length(d));
sum_rate_ub = zeros(1, length(d));
ic_avg   = zeros(1, length(d));
ic_g_avg = zeros(1, length(d));
cp_avg = zeros(1, length(d));
% Read and process
for i = 1:length(d)
    for j = 1:N_r
        str = append('D:/Distance_GW2/data_fix_pow_near/data_d_', num2str(d(i)*10), '_num_', num2str(j));
        load(str)
        % Find parameters
        [r1, r2] = find_distance(N, s, S);
        J_overlap = find_overlap(r1, r2, R, N, cp);
        cp_avg(i) = cp_avg(i) + sum(sum(cp(J_overlap,:)))/1000;
        Tij = Tij_table(SF-6)';
        % Initial: xij = 0 for overlap area
        X = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
        X(J_overlap,:) = zeros(length(J_overlap),2);
        th1_tmp = N_D*T - a*sum(Tij.*X(:,1).*cp(:,1));
        th2_tmp = N_D*T - a*sum(Tij.*X(:,2).*cp(:,2));
        th1 = max(th1_tmp, 0);
        th2 = max(th2_tmp, 0);
        % Grouping
        g1 = J_overlap(1:2:end);
        g2 = J_overlap(2:2:end);
%         % Grouping
%         [g1, g2, g3, g4, g5, g6] = grouping(J_overlap, SF);
%         [p1, p2, p3, p4, p5, p6] = find_proportion(g1, g2, g3, g4, g5, g6);
%         G1 = [g1;g2;g3;g4;g5];
%         G2 = g6;
%         P1 = p1+p2+p3+p4+p5;
%         P2 = p6;
        % Auction algorithm (Output is an array)
        [obj_tmp, ic_tmp, X_ng_tmp] = auction_algorithm(N_i, epsilon, J_overlap, N, a, cp, small_value, Tij, th1, th2, beta);
        [obj_tmp1, ic_tmp1, X_g1_tmp] = auction_algorithm(N_i, epsilon, g1, N, a, cp, small_value, Tij, 0.5*th1, 0.5*th2, beta);
        [obj_tmp2, ic_tmp2, X_g2_tmp] = auction_algorithm(N_i, epsilon, g2, N, a, cp, small_value, Tij, 0.5*th1, 0.5*th2, beta);
%         [obj_tmp3, ic_tmp3, X_g3_tmp] = auction_algorithm(N_i, epsilon, g3, N, a, cp, small_value, Tij, p3*th1, p3*th2, beta);
%         [obj_tmp4, ic_tmp4, X_g4_tmp] = auction_algorithm(N_i, epsilon, g4, N, a, cp, small_value, Tij, p4*th1, p4*th2, beta);
%         [obj_tmp5, ic_tmp5, X_g5_tmp] = auction_algorithm(N_i, epsilon, g5, N, a, cp, small_value, Tij, p5*th1, p5*th2, beta);
%         [obj_tmp6, ic_tmp6, X_g6_tmp] = auction_algorithm(N_i, epsilon, g6, N, a, cp, small_value, Tij, p6*th1, p6*th2, beta);
        % Find benchmarks (Output is a number)
        ran_ed = zeros(1, length(J_overlap));
        ran_gw = zeros(1,length(J_overlap));
        for k = 1:length(J_overlap)
            ran_ed(k) = randi([1 length(J_overlap)-k+1]);
            ran_gw(k) = randi([1 2]);
        end
        [obj_bm1_tmp, X_bm1] = benchmark_1(cp, J_overlap, Tij, N_D, T, N, a, ran_ed);
        [obj_bm2_tmp, X_bm2] = benchmark_2(cp, J_overlap, Tij, N_D, T, N, a, ran_ed, ran_gw);
        [obj_bm3_tmp, X_ub] = benchmark_3(N, J_overlap, cp, a);
        % Sum metrics
%         ic_g_tmp = max([ic_tmp1 ic_tmp2 ic_tmp3 ic_tmp4 ic_tmp5 ic_tmp6]);
        ic_g_tmp = max([ic_tmp1 ic_tmp2]);
        % Save output
        X_ng = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
        X_ng(J_overlap,:) = X_ng_tmp(J_overlap,:);
        X_g = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
        X_g(g1,:) = X_g1_tmp(g1,:);
        X_g(g2,:) = X_g2_tmp(g2,:);
%         X_g(g3,:) = X_g3_tmp(g3,:);
%         X_g(g4,:) = X_g4_tmp(g4,:);
%         X_g(g5,:) = X_g5_tmp(g5,:);
%         X_g(g6,:) = X_g6_tmp(g6,:);

        obj(i)     = obj(i) + a*sum(sum(X_ng.*cp));
        obj_g(i)   = obj_g(i) + a*sum(sum(X_g.*cp));
        obj_bm1(i) = obj_bm1(i) + a*sum(sum(X_bm1.*cp));
        obj_bm2(i) = obj_bm2(i) + a*sum(sum(X_bm2.*cp));
        obj_ub(i)  = obj_ub(i) + a*sum(sum(X_ub.*cp));
        
        br = bit_rate_table(SF - 6);
        sum_rate(i)     = sum_rate(i) + a*sum(sum(X_ng.*cp.*[br br]));
        sum_rate_g(i)   = sum_rate_g(i) + a*sum(sum(X_g.*cp.*[br br]));
        sum_rate_bm1(i) = sum_rate_bm1(i) + a*sum(sum(X_bm1.*cp.*[br br]));
        sum_rate_bm2(i) = sum_rate_bm2(i) + a*sum(sum(X_bm2.*cp.*[br br]));
        sum_rate_ub(i)  = sum_rate_ub(i) + a*sum(sum(X_ub.*cp.*[br br]));

        ic_avg(i) = ic_avg(i) + ic_tmp;
        ic_g_avg(i) = ic_g_avg(i) + ic_g_tmp;
    end
end
obj = obj/N_r;
obj_g = obj_g/N_r;
obj_bm1 = obj_bm1/N_r;
obj_bm2 = obj_bm2/N_r;
obj_ub = obj_ub/N_r;

sum_rate = sum_rate/N_r;
sum_rate_g = sum_rate_g/N_r;
sum_rate_bm1 = sum_rate_bm1/N_r;
sum_rate_bm2 = sum_rate_bm2/N_r;
sum_rate_ub = sum_rate_ub/N_r;

ic_avg = ic_avg/N_r;
ic_g_avg = ic_g_avg/N_r;

% Plot objective function
figure(1)
plot(d, obj_ub, '-^r', 'LineWidth', 1.5); hold on
plot(d, obj_bm1, '-*k', 'LineWidth', 1.5); hold on
plot(d, obj_bm2, '--*k', 'LineWidth', 1.5); hold on
plot(d, obj, '-ob', 'LineWidth', 1.5); hold on
plot(d, obj_g, '--ob', 'LineWidth', 1.5); hold off
grid on
axis([1 2 0 14])
xlabel('Distance (km)')
ylabel('Utility function')
legend('Upper bound', 'Benchmark 1', 'Benchmark 2', 'Non-grouping', 'Grouping')

% Plot sum rate
figure(2)
plot(d, sum_rate_ub, '-^r', 'LineWidth', 1.5); hold on
plot(d, sum_rate_bm1, '-*k', 'LineWidth', 1.5); hold on
plot(d, sum_rate_bm2, '--*k', 'LineWidth', 1.5); hold on
plot(d, sum_rate, '-ob', 'LineWidth', 1.5); hold on
plot(d, sum_rate_g, '--ob', 'LineWidth', 1.5); hold off
axis([1 2 0 50])
grid on
xlabel('Distance (km)')
ylabel('Sum rate (kb/s)')
legend('Upper bound', 'Benchmark 1', 'Benchmark 2', 'Non-grouping', 'Grouping')

% Plot number of iteration
figure(3)
plot(d, ic_avg, '-ob', 'LineWidth', 1.5); hold on
plot(d, ic_g_avg, '--ob', 'LineWidth', 1.5); hold off
axis([1 2 0 50])
grid on
xlabel('Distance (km)')
ylabel('Number of iteration')
legend('Non-grouping', 'Grouping')

