clear
% Parameter
d = 10:1:20;
N_r = 200;
a = 0.1;
bit_rate_table = [5.47 3.13 1.76 0.98 0.54 0.29]';
% Initialization
obj     = zeros(1, length(d));
obj_g   = zeros(1, length(d));
obj_bm1 = zeros(1, length(d));
obj_bm2 = zeros(1, length(d));
obj_ub  = zeros(1, length(d));
sum_rate     = zeros(1, length(d));
sum_rate_g   = zeros(1, length(d));
sum_rate_bm1 = zeros(1, length(d));
sum_rate_bm2 = zeros(1, length(d));
sum_rate_ub = zeros(1, length(d));
ic_avg   = zeros(1, length(d));
ic_g_avg = zeros(1, length(d));
% for i = 1:12
%     for j = 1:N_r
%         str = append('D:/Distance_GW2/data_2_13_eps_1/data_d_', num2str(d(i)), '_num_', num2str(j));
%         load(str)
%         obj(i)     = obj(i) + a*sum(sum(X_ng.*cp));
%         obj_g(i)   = obj_g(i) + a*sum(sum(X_g.*cp));
%         obj_bm1(i) = obj_bm1(i) + a*sum(sum(X_bm1.*cp));
%         obj_bm2(i) = obj_bm2(i) + a*sum(sum(X_bm2.*cp));
%         obj_ub(i)  = obj_ub(i) + a*sum(sum(X_ub.*cp));
%         br = bit_rate_table(SF - 6);
%         sum_rate(i)     = sum_rate(i) + a*sum(sum(X_ng.*cp.*[br br]));
%         sum_rate_g(i)   = sum_rate_g(i) + a*sum(sum(X_g.*cp.*[br br]));
%         sum_rate_bm1(i) = sum_rate_bm1(i) + a*sum(sum(X_bm1.*cp.*[br br]));
%         sum_rate_bm2(i) = sum_rate_bm2(i) + a*sum(sum(X_bm2.*cp.*[br br]));
%         sum_rate_ub(i)  = sum_rate_ub(i) + a*sum(sum(X_ub.*cp.*[br br]));
%         if j==200
%             ic_avg(i) = ic;
%             ic_g_avg(i) = ic_g;
%         elseif j>200
%             ic_avg(i) = ic_avg(i) + ic_tmp;
%             ic_g_avg(i) = ic_g_avg(i) + ic_g_tmp;
%         end
%     end
% end
% for i = 13:length(d)
%     for j = 1:N_r
%         str = append('D:/Distance_GW2/data_14_20_eps_1/data_d_', num2str(d(i)), '_num_', num2str(j));
%         load(str)
%         obj(i)     = obj(i) + a*sum(sum(X_ng.*cp));
%         obj_g(i)   = obj_g(i) + a*sum(sum(X_g.*cp));
%         obj_bm1(i) = obj_bm1(i) + a*sum(sum(X_bm1.*cp));
%         obj_bm2(i) = obj_bm2(i) + a*sum(sum(X_bm2.*cp));
%         obj_ub(i)  = obj_ub(i) + a*sum(sum(X_ub.*cp));
%         br = bit_rate_table(SF - 6);
%         sum_rate(i)     = sum_rate(i) + a*sum(sum(X_ng.*cp.*[br br]));
%         sum_rate_g(i)   = sum_rate_g(i) + a*sum(sum(X_g.*cp.*[br br]));
%         sum_rate_bm1(i) = sum_rate_bm1(i) + a*sum(sum(X_bm1.*cp.*[br br]));
%         sum_rate_bm2(i) = sum_rate_bm2(i) + a*sum(sum(X_bm2.*cp.*[br br]));
%         sum_rate_ub(i)  = sum_rate_ub(i) + a*sum(sum(X_ub.*cp.*[br br]));
%         ic_avg(i) =  ic_avg(i) + ic_tmp;
%         ic_g_avg(i) = ic_g_avg(i) + ic_g_tmp;
%     end
% end
% obj = obj/N_r;
% obj_g = obj_g/N_r;
% obj_bm1 = obj_bm1/N_r;
% obj_bm2 = obj_bm2/N_r;
% obj_ub = obj_ub/N_r;
% 
% sum_rate = sum_rate/N_r;
% sum_rate_g = sum_rate_g/N_r;
% sum_rate_bm1 = sum_rate_bm1/N_r;
% sum_rate_bm2 = sum_rate_bm2/N_r;
% sum_rate_ub = sum_rate_ub/N_r;
% 
% ic_avg(1:12) = ic_avg(1:12)/N_r;
% ic_avg(13:19) = ic_avg(13:19)/N_r;
% ic_g_avg(1:12) = ic_g_avg(1:12)/N_r;
% ic_g_avg(13:19) = ic_g_avg(13:19)/N_r;

for i = 1:length(d)
    for j = 1:N_r
        str = append('D:/Distance_GW2/data_var_pow/data_d_', num2str(d(i)), '_num_', num2str(j));
        load(str)
        obj(i)     = obj(i) + a*sum(sum(X_ng.*cp));
        obj_g(i)   = obj_g(i) + a*sum(sum(X_g.*cp));
        obj_bm1(i) = obj_bm1(i) + a*sum(sum(X_bm1.*cp));
        obj_bm2(i) = obj_bm2(i) + a*sum(sum(X_bm2.*cp));
        obj_ub(i)  = obj_ub(i) + a*sum(sum(X_ub.*cp));
        br = bit_rate_table(SF - 6);
        sum_rate(i)     = sum_rate(i) + a*sum(sum(X_ng.*cp.*[br br]));
        sum_rate_g(i)   = sum_rate_g(i) + a*sum(sum(X_g.*cp.*[br br]));
        sum_rate_bm1(i) = sum_rate_bm1(i) + a*sum(sum(X_bm1.*cp.*[br br]));
        sum_rate_bm2(i) = sum_rate_bm2(i) + a*sum(sum(X_bm2.*cp.*[br br]));
        sum_rate_ub(i)  = sum_rate_ub(i) + a*sum(sum(X_ub.*cp.*[br br]));
        ic_avg(i) = ic_avg(i) + ic_tmp;
        ic_g_avg(i) = ic_g_avg(i) + ic_g_tmp;
    end
end
obj = obj/N_r;
obj_g = obj_g/N_r;
obj_bm1 = obj_bm1/N_r;
obj_bm2 = obj_bm2/N_r;
obj_ub = obj_ub/N_r;

sum_rate = sum_rate/N_r;
sum_rate_g = sum_rate_g/N_r;
sum_rate_bm1 = sum_rate_bm1/N_r;
sum_rate_bm2 = sum_rate_bm2/N_r;
sum_rate_ub = sum_rate_ub/N_r;

ic_avg = ic_avg/N_r;
ic_g_avg = ic_g_avg/N_r;

% Plot objective function
figure(1)
plot(d, obj_ub, '-or', 'LineWidth', 1.5); hold on
plot(d, obj_bm1, '--or', 'LineWidth', 1.5); hold on
plot(d, obj_bm2, '-.or', 'LineWidth', 1.5); hold on
plot(d, obj, '--ob', 'LineWidth', 1.5); hold on
plot(d, obj_g, '-ob', 'LineWidth', 1.5); hold off
grid on
xlabel('Distance (km)')
ylabel('Objective function')
legend('Upper bound', 'Benchmark 1', 'Benchmark 2', 'Non-grouping', 'Grouping')

% Plot sum rate
figure(2)
plot(d, sum_rate_ub, '-or', 'LineWidth', 1.5); hold on
plot(d, sum_rate_bm1, '--or', 'LineWidth', 1.5); hold on
plot(d, sum_rate_bm2, '-.or', 'LineWidth', 1.5); hold on
plot(d, sum_rate, '--ob', 'LineWidth', 1.5); hold on
plot(d, sum_rate_g, '-ob', 'LineWidth', 1.5); hold off
grid on
xlabel('Distance (km)')
ylabel('Sum rate')
legend('Upper bound', 'Benchmark 1', 'Benchmark 2', 'Non-grouping', 'Grouping')

% Plot number of iteration
figure(3)
plot(d, ic_avg, '--ob', 'LineWidth', 1.5); hold on
plot(d, ic_g_avg, '-ob', 'LineWidth', 1.5); hold off
grid on
xlabel('Distance (km)')
ylabel('Number of iteration')
legend('Non-grouping', 'Grouping')

% % Plot objective function
% figure(1)
% plot(d(13:19), obj_ub(13:19), '-or', 'LineWidth', 1.5); hold on
% plot(d(13:19), obj_bm1(13:19), '--or', 'LineWidth', 1.5); hold on
% plot(d(13:19), obj_bm2(13:19), '-.or', 'LineWidth', 1.5); hold on
% plot(d(13:19), obj(13:19), '--ob', 'LineWidth', 1.5); hold on
% plot(d(13:19), obj_g(13:19), '-ob', 'LineWidth', 1.5); hold off
% grid on
% xlabel('Distance (km)')
% ylabel('Objective function')
% legend('Upper bound', 'Benchmark 1', 'Benchmark 2', 'Non-grouping', 'Grouping')
% 
% % Plot sum rate
% figure(2)
% plot(d(13:19), sum_rate_ub(13:19), '-or', 'LineWidth', 1.5); hold on
% plot(d(13:19), sum_rate_bm1(13:19), '--or', 'LineWidth', 1.5); hold on
% plot(d(13:19), sum_rate_bm2(13:19), '-.or', 'LineWidth', 1.5); hold on
% plot(d(13:19), sum_rate(13:19), '--ob', 'LineWidth', 1.5); hold on
% plot(d(13:19), sum_rate_g(13:19), '-ob', 'LineWidth', 1.5); hold off
% grid on
% xlabel('Distance (km)')
% ylabel('Sum rate')
% legend('Upper bound', 'Benchmark 1', 'Benchmark 2', 'Non-grouping', 'Grouping')
% 
% % Plot number of iteration
% figure(3)
% plot(d(13:19), ic_avg(13:19), '--ob', 'LineWidth', 1.5); hold on
% plot(d(13:19), ic_g_avg(13:19), '-ob', 'LineWidth', 1.5); hold off
% grid on
% xlabel('Distance (km)')
% ylabel('Number of iteration')
% legend('Non-grouping', 'Grouping')
