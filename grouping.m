function [g1, g2, g3, g4, g5, g6] = grouping(J_overlap, SF)
    g1 = zeros(length(J_overlap),1);
    g2 = zeros(length(J_overlap),1);
    g3 = zeros(length(J_overlap),1);
    g4 = zeros(length(J_overlap),1);
    g5 = zeros(length(J_overlap),1);
    g6 = zeros(length(J_overlap),1);
    g1_idx = 0;
    g2_idx = 0;
    g3_idx = 0;
    g4_idx = 0;
    g5_idx = 0;
    g6_idx = 0;
    for i = 1:length(J_overlap)
        if SF(J_overlap(i))==7
            g1_idx = g1_idx + 1;
            g1(g1_idx) = J_overlap(i);
        elseif SF(J_overlap(i))==8
            g2_idx = g2_idx + 1;
            g2(g2_idx) = J_overlap(i);
        elseif SF(J_overlap(i))==9
            g3_idx = g3_idx + 1;
            g3(g3_idx) = J_overlap(i);
        elseif SF(J_overlap(i))==10
            g4_idx = g4_idx + 1;
            g4(g4_idx) = J_overlap(i);
        elseif SF(J_overlap(i))==11
            g5_idx = g5_idx + 1;
            g5(g5_idx) = J_overlap(i);
        elseif SF(J_overlap(i))==12
            g6_idx = g6_idx + 1;
            g6(g6_idx) = J_overlap(i);
        else
            disp('error')
        end
    end
    g1 = g1(1:g1_idx);
    g2 = g2(1:g2_idx);
    g3 = g3(1:g3_idx);
    g4 = g4(1:g4_idx);
    g5 = g5(1:g5_idx);
    g6 = g6(1:g6_idx);
end