clear;
% Parameter
N = 500;
N_G = 2;
N_E = (N/N_G)*ones(2,1);
R = 1;
N_D = 8;
a = 0.1;
small_value = -100;
Tij_table = [0.34842 0.61491 0.61541 0.61645 1.31482 2.46579];
T = 0.05;
N_i = 1000; % Number of iteration (in auction algorithm)
epsilon = 1;
N_r = 1; % Number of random
beta = 0.0001;
bit_rate_table = [5.47 3.13 1.76 0.98 0.54 0.29]';
d = 1.4;
% j = randi([1 100]);

% str = append('D:/Distance_GW2/data_var_pow_test/data_d_', num2str(d), '_num_', num2str(j));
% load(str)
S = [10 10;10+d 10];
s = coordinate_E(N_E, N_G, S, R);
[r1, r2] = find_distance(N, s, S);
SF = zeros(N,1);
for i = 1:N
    SF(i) = find_SF(r1(i), r2(i), R);
end

% Find parameters
s7 = zeros(N,2);
s8 = zeros(N,2);
s9 = zeros(N,2);
s10 = zeros(N,2);
s11 = zeros(N,2);
s12 = zeros(N,2);
i7 = 0;
i8 = 0;
i9 = 0;
i10 = 0;
i11 = 0;
i12 = 0;
for i=1:N
    if SF(i)==7
        i7 = i7 + 1;
        s7(i7,:) = s(i,:);
    elseif SF(i)==8
        i8 = i8 + 1;
        s8(i8,:) = s(i,:);
    elseif SF(i)==9
        i9 = i9 + 1;
        s9(i9,:) = s(i,:);
    elseif SF(i)==10
        i10 = i10 + 1;
        s10(i10,:) = s(i,:);
    elseif SF(i)==11
        i11 = i11 + 1;
        s11(i11,:) = s(i,:);
    else
        i12 = i12 + 1;
        s12(i12,:) = s(i,:);
    end
end
s7 = s7(1:i7,:);
s8 = s8(1:i8,:);
s9 = s9(1:i9,:);
s10 = s10(1:i10,:);
s11 = s11(1:i11,:);
s12 = s12(1:i12,:);

t = 0:0.001:2*pi;
cov = [R*cos(t)' R*sin(t)'];
cov_1 = zeros(length(t), 2);
cov_2 = zeros(length(t), 2);
for i = 1:length(t)
    cov_1(i,:) = cov(i,:) + S(1,:);
    cov_2(i,:) = cov(i,:) + S(2,:);
end

figure(1)
scatter(s7(:,1), s7(:,2), 'xm', 'LineWIdth', 1.5); hold on
scatter(s8(:,1), s8(:,2), 'om', 'LineWIdth', 1.5); hold on
scatter(s9(:,1), s9(:,2), 'xg', 'LineWIdth', 1.5); hold on
scatter(s10(:,1), s10(:,2), 'og', 'LineWIdth', 1.5); hold on
scatter(s11(:,1), s11(:,2), 'xb', 'LineWIdth', 1.5); hold on
scatter(s12(:,1), s12(:,2), 'ob', 'LineWIdth', 1.5); hold on
scatter(cov_1(:,1), cov_1(:,2), '.r', 'LineWidth', 1.5); hold on
scatter(cov_2(:,1), cov_2(:,2), '.r', 'LineWidth', 1.5); hold on
scatter(S(:,1), S(:,2), '^r', 'LineWidth', 1.5); hold off
axis equal
grid on
legend('SF7', 'SF8', 'SF9', 'SF10', 'SF11', 'SF12')
