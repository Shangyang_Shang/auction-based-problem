function t = col_time(Ti, Tj, SF, Tij_table, N)
    % Ti: desired signal
    % Tj: interference
    % T: boundary of delay
    N_SF = [sum(SF==7) sum(SF==8) sum(SF==9) sum(SF==10) sum(SF==11) sum(SF==12)];
    T = sum(N_SF.*Tij_table)/N;
    if Ti>Tj
        t = (Tj^2)/(2*Ti/T);
    else % Ta(i)<=Ta(j)
        t = Ti/(2*T);
    end
end