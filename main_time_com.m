clear;
% Parameter
d = 1.4;
N = 500;
R = 1;
N_D = 8;
a = 0.1;
small_value = -100;
Tij_table = [0.34842 0.61491 0.61541 0.61645 1.31482 2.46579];
T = 0.2;
N_i = 1000; % Number of iteration (in auction algorithm)
epsilon = 1;
N_rs = 10; % Number of random
beta = 0.0001;
bit_rate_table = [5.47 3.13 1.76 0.98 0.54 0.29]';

sample = 1:5:31;
t0 = zeros(1, length(sample));
t1 = zeros(1, length(sample));
t2 = zeros(1, length(sample));
t3 = zeros(1, length(sample));
for iter = 1:N_rs
    for i = 1:length(sample)
        for j = (1:sample(i))+(iter-1)*31
            str = append('D:/Distance_GW2/data_fix_pow_near/data_d_', num2str(d*10), '_num_', num2str(j));
            load(str)
            [r1, r2] = find_distance(N, s, S);
            J_overlap = find_overlap(r1, r2, R, N, cp);
            Tij = Tij_table(SF-6)';
            % Initial: xij = 0 for overlap area
            X = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
            X(J_overlap,:) = zeros(length(J_overlap),2);
            th1_tmp = N_D*T - a*sum(Tij.*X(:,1).*cp(:,1));
            th2_tmp = N_D*T - a*sum(Tij.*X(:,2).*cp(:,2));
            th1 = max(th1_tmp, 0);
            th2 = max(th2_tmp, 0);
            % Grouping
            g1 = J_overlap(1:2:end);
            g2 = J_overlap(2:2:end);
            % Auction algorithm (Output is an array)
            tic
            [obj_tmp, ic_tmp, X_ng_tmp] = auction_algorithm(N_i, epsilon, J_overlap, N, a, cp, small_value, Tij, th1, th2, beta);
            t0(i) = t0(i) + toc/ic_tmp;
            tic
            [obj_tmp1, ic_tmp1, X_g1_tmp] = auction_algorithm(N_i, epsilon, g1, N, a, cp, small_value, Tij, 0.5*th1, 0.5*th2, beta);
            t1_1 = toc/ic_tmp1;
            tic
            [obj_tmp2, ic_tmp2, X_g2_tmp] = auction_algorithm(N_i, epsilon, g2, N, a, cp, small_value, Tij, 0.5*th1, 0.5*th2, beta);
            t1_2 = toc/ic_tmp2;
            t1(i) = t1(i) + max([t1_1 t1_2]);
            % Find benchmarks (Output is a number)
            tic
            [obj_bm1_tmp, X_bm1, n_bm1] = benchmark_1(cp, J_overlap, Tij, N_D, T, N, a);
            t2(i) = t2(i) + toc/n_bm1;
            tic
            [obj_bm2_tmp, X_bm2, n_bm2] = benchmark_2(cp, J_overlap, Tij, N_D, T, N, a);
            t3(i) = t3(i) + toc/n_bm2;
        end
    end
end

t0 = t0/N_rs;
t1 = t1/N_rs;
t2 = t2/N_rs;
t3 = t3/N_rs;

figure(1)
plot(sample, t2*1e6, '-ok', 'LineWidth', 1.5); hold on
plot(sample, t3*1e6, '--ok', 'LineWidth', 1.5); hold on
plot(sample, t0*1e6, '-ob', 'LineWidth', 1.5); hold on
plot(sample, t1*1e6, '--ob', 'LineWidth', 1.5); hold off
grid on
% axis([1 31 min([t0(1) t1(1) t2(1) t3(1)])*1e6 max([t0(7) t1(7) t2(7) t3(7)])*1e6])
axis([1 31 0 300])
xlabel('Number of samples')
ylabel('Time per iteration (\mus)')
legend('Benchmark 1', 'Benchmark 2', 'Non-grouping', 'Grouping')