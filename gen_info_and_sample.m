% Generate information and sample for different distance
clear;
% N = 5000;
% N_G = 2;
% N_E = (N/N_G)*ones(2,1);
% R = 1;
% d = [1.1:0.1:1.3 1.5 1.7:0.1:2];
% alpha = 3;
% gamma_table_dB = [-6 -9 -12 -15 -17.5 -20];
% gamma_table = 10.^(gamma_table_dB./10);
% Tij_table = [0.34842 0.61491 0.61541 0.61645 1.31482 2.46579];
% % 930 sec for one distance
% for i = 1:length(d)
%     tic
%     disp(['d = ' num2str(d(i))])
%     % Position of gateways
%     S = [10 10;10+d(i) 10];
%     % Position of end-devices
%     s = coordinate_E(N_E, N_G, S, R);
%     % Find parameters
%     [r1, r2] = find_distance(N, s, S);
%     [cp, SF, Tij] = find_capture_prob(r1, r2, d(i)+R, N, Tij_table, gamma_table, alpha);
%     str = append('D:/opt_prob_data/fix_pow_near/d_',num2str(d(i)*10),'.mat');
%     save(str, 'cp', 'SF', 's', 'S')
%     toc
% end

N = 500;
N_G = 2;
N_E = (N/N_G)*ones(2,1);
R = 1;
d = [1:0.1:1.3 1.5:0.1:2];
alpha = 3;
gamma_table_dB = [-6 -9 -12 -15 -17.5 -20];
gamma_table = 10.^(gamma_table_dB./10);
Tij_table = [0.34842 0.61491 0.61541 0.61645 1.31482 2.46579];
Ns = 1000;
% d_idx: index of distance
% s_idx: index of sample
for d_idx = 1:length(d)
    S = [10 10;10+d(d_idx) 10];
    load(['D:/opt_prob_data/fix_pow_near/d_' num2str(d(d_idx)*10) '.mat'])
    s_sel = zeros(500, 2);
    for s_idx = 1:Ns
        disp(['distance = ' num2str(d(d_idx)) ', sample = ' num2str(s_idx)])
        tic
        sel_set1 = randsample(1:2500, 250);
        sel_set2 = randsample(2501:5000, 250);
        s_sel(1:250,:) = s(sel_set1,:);
        s_sel(251:500,:) = s(sel_set2,:);
        [r1j, r2j] = find_distance(N, s_sel, S);
        [cp_sel, SF_sel, ~] = find_capture_prob(r1j, r2j, d(d_idx)+R, N, Tij_table, gamma_table, alpha);
        str = append('D:/opt_prob_data/fix_pow_near/d_',num2str(d(d_idx)*10),'/',num2str(s_idx),'.mat');
        save(str, 'S', 's_sel', 'cp_sel', 'SF_sel')
        toc
    end
end
