function cp = capture_prob(rj, Pt, gamma, col_t, i, alpha)
    cp1 = prod(1./(gamma.*(Pt./Pt(i)).*((rj./rj(i)).^(-alpha)).*col_t+1))/(1/(gamma*col_t(1)+1));
    sigma2_dBm = -174 + 10*log10(125000);
    sigma2 = 10^(sigma2_dBm/10)*0.001;
    cp2 = exp(-gamma*sigma2/(Pt(i)*(rj(i)^(-alpha))*0.1));
    cp = cp1*cp2;
end