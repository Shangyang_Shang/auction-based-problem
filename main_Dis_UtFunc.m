% Distance - Utility function
% Distance - Sum rate
clear;
% Parameter
d = 1:0.1:2;
N = 500;
R = 10;
N_D = 8;
a = 1;
small_value = -100;
Tij_table = [0.34842 0.61491 0.61541 0.61645 1.31482 2.46579];
T = 0.2;
N_i = 1000; % Number of iteration (in auction algorithm)
epsilon = 1;
Ns = 1e3; % Number of random
beta = 0.0001;
bit_rate_table = [5.47 3.13 1.76 0.98 0.54 0.29]';
% Initialization
obj     = zeros(1, length(d));
obj_g   = zeros(1, length(d));
obj_bm1 = zeros(1, length(d));
obj_bm2 = zeros(1, length(d));
obj_ub  = zeros(1, length(d));
sum_rate     = zeros(1, length(d));
sum_rate_g   = zeros(1, length(d));
sum_rate_bm1 = zeros(1, length(d));
sum_rate_bm2 = zeros(1, length(d));
sum_rate_ub = zeros(1, length(d));
ic_avg   = zeros(1, length(d));
ic_g_avg = zeros(1, length(d));
cp_avg = zeros(1, length(d));
% Read and process
for i = 1:length(d)
    disp(['distance: ' num2str(d(i))])
    for s_idx = 1:Ns
        %str = append('D:/Distance_GW2/data_fix_pow_near/data_d_', num2str(d(i)*10), '_num_', num2str(j));
        str = append('D:/opt_prob_data/fix_pow_near/d_', num2str(d(i)*10), '/', num2str(s_idx), '.mat');
        load(str)
        % Find parameters
        [r1j, r2j] = find_distance(N, s_sel, S);
        J_overlap = find_overlap(r1j, r2j, R, N, cp_sel);
        cp_avg(i) = cp_avg(i) + sum(sum(cp_sel(J_overlap,:)))/1000;
        Tij = Tij_table(SF_sel-6)';
        % Initial: xij = 0 for overlap area
        X = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
        X(J_overlap,:) = zeros(length(J_overlap),2);
        th1_tmp = N_D*T - a*sum(Tij.*X(:,1).*cp_sel(:,1));
        th2_tmp = N_D*T - a*sum(Tij.*X(:,2).*cp_sel(:,2));
        th1 = max(th1_tmp, 0);
        th2 = max(th2_tmp, 0);
        % Grouping
        g1 = J_overlap(1:2:end);
        g2 = J_overlap(2:2:end);
        % Auction algorithm (Output is an array)
        [obj_tmp, ic_tmp, X_ng_tmp] = auction_algorithm(N_i, epsilon, J_overlap, N, a, cp_sel, small_value, Tij, th1, th2, beta);
        [obj_tmp1, ic_tmp1, X_g1_tmp] = auction_algorithm(N_i, epsilon, g1, N, a, cp_sel, small_value, Tij, 0.5*th1, 0.5*th2, beta);
        [obj_tmp2, ic_tmp2, X_g2_tmp] = auction_algorithm(N_i, epsilon, g2, N, a, cp_sel, small_value, Tij, 0.5*th1, 0.5*th2, beta);
        % Find benchmarks (Output is a number)
        ran_ed = zeros(1, length(J_overlap));
        ran_gw = zeros(1,length(J_overlap));
        for k = 1:length(J_overlap)
            ran_ed(k) = randi([1 length(J_overlap)-k+1]);
            ran_gw(k) = randi([1 2]);
        end
        %[obj_bm1_tmp, X_bm1] = benchmark_1(cp, J_overlap, Tij, N_D, T, N, a, ran_ed);
        [obj_bm1_tmp, X_bm1] = benchmark_1(cp_sel, J_overlap, Tij, N_D, T, N, a, ran_ed);
        %[obj_bm1_tmp, X_bm1] = benchmark_1(cp_sel, J_overlap, Tij, N_D, avail_time(t_idx), N, a, ran_ed);
        %[obj_bm2_tmp, X_bm2] = benchmark_2(cp, J_overlap, Tij, N_D, T, N, a, ran_ed, ran_gw);
        [obj_bm2_tmp, X_bm2, n_bm2] = benchmark_2(cp_sel, J_overlap, Tij, N_D, T, N, a, ran_ed, ran_gw);
        %[obj_bm2_tmp, X_bm2, n_bm2] = benchmark_2(cp_sel, J_overlap, Tij, N_D, avail_time(t_idx), N, a, ran_ed, ran_gw);
        [obj_bm3_tmp, X_ub] = benchmark_3(N, J_overlap, cp_sel, a);
        % Sum metrics
        ic_g_tmp = max([ic_tmp1 ic_tmp2]);
        % Save output
        X_ng = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
        X_ng(J_overlap,:) = X_ng_tmp(J_overlap,:);
        X_g = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
        X_g(g1,:) = X_g1_tmp(g1,:);
        X_g(g2,:) = X_g2_tmp(g2,:);

        obj(i)     = obj(i) + a*sum(sum(X_ng.*cp_sel));
        obj_g(i)   = obj_g(i) + a*sum(sum(X_g.*cp_sel));
        obj_bm1(i) = obj_bm1(i) + a*sum(sum(X_bm1.*cp_sel));
        obj_bm2(i) = obj_bm2(i) + a*sum(sum(X_bm2.*cp_sel));
        obj_ub(i)  = obj_ub(i) + a*sum(sum(X_ub.*cp_sel));
        
        br = bit_rate_table(SF_sel - 6);
        sum_rate(i)     = sum_rate(i) + a*sum(sum(X_ng.*cp_sel.*[br br]));
        sum_rate_g(i)   = sum_rate_g(i) + a*sum(sum(X_g.*cp_sel.*[br br]));
        sum_rate_bm1(i) = sum_rate_bm1(i) + a*sum(sum(X_bm1.*cp_sel.*[br br]));
        sum_rate_bm2(i) = sum_rate_bm2(i) + a*sum(sum(X_bm2.*cp_sel.*[br br]));
        sum_rate_ub(i)  = sum_rate_ub(i) + a*sum(sum(X_ub.*cp_sel.*[br br]));

        ic_avg(i) = ic_avg(i) + ic_tmp;
        ic_g_avg(i) = ic_g_avg(i) + ic_g_tmp;
    end
end
obj = obj/Ns;
obj_g = obj_g/Ns;
obj_bm1 = obj_bm1/Ns;
obj_bm2 = obj_bm2/Ns;
obj_ub = obj_ub/Ns;

sum_rate = sum_rate/Ns;
sum_rate_g = sum_rate_g/Ns;
sum_rate_bm1 = sum_rate_bm1/Ns;
sum_rate_bm2 = sum_rate_bm2/Ns;
sum_rate_ub = sum_rate_ub/Ns;

ic_avg = ic_avg/Ns;
ic_g_avg = ic_g_avg/Ns;

% Moving average
num = 5;
obj_ub = movmean(obj_ub,num);
obj_bm1 = movmean(obj_bm1,num);
obj_bm2 = movmean(obj_bm2,num);
obj = movmean(obj,num);
obj_g = movmean(obj_g,num);
sum_rate_ub = movmean(sum_rate_ub,num);
sum_rate_bm1 = movmean(sum_rate_bm1,num);
sum_rate_bm2 = movmean(sum_rate_bm2,num);
sum_rate = movmean(sum_rate,num);
sum_rate_g = movmean(sum_rate_g,num);

% Plot objective function
figure(1)
plot(d, obj_ub, '-^r', 'LineWidth', 1.5); hold on
plot(d, obj_bm1, '-*k', 'LineWidth', 1.5); hold on
plot(d, obj_bm2, '--*k', 'LineWidth', 1.5); hold on
plot(d, obj, '-ob', 'LineWidth', 1.5); hold on
plot(d, obj_g, '--ob', 'LineWidth', 1.5); hold off
grid on
% axis([1 2 0 14])
xlabel('Distance (km)')
ylabel('Utility function')
legend('Upper bound', 'Benchmark 1', 'Benchmark 2', 'Non-grouping', 'Grouping')

% Plot sum rate
figure(2)
plot(d, sum_rate_ub, '-^r', 'LineWidth', 1.5); hold on
plot(d, sum_rate_bm1, '-*k', 'LineWidth', 1.5); hold on
plot(d, sum_rate_bm2, '--*k', 'LineWidth', 1.5); hold on
plot(d, sum_rate, '-ob', 'LineWidth', 1.5); hold on
plot(d, sum_rate_g, '--ob', 'LineWidth', 1.5); hold off
% axis([1 2 0 50])
grid on
xlabel('Distance (km)')
ylabel('Sum rate (kb/s)')
legend('Upper bound', 'Benchmark 1', 'Benchmark 2', 'Non-grouping', 'Grouping')

% % Plot number of iteration
% figure(3)
% plot(d, ic_avg, '-ob', 'LineWidth', 1.5); hold on
% plot(d, ic_g_avg, '--ob', 'LineWidth', 1.5); hold off
% grid on
% xlabel('Distance (km)')
% ylabel('Number of iteration')
% legend('Non-grouping', 'Grouping')

