function [obj, X, n] = benchmark_2(cp, J_overlap, Tij, N_D, T, N, a, ran_ed, ran_gw)
    %X = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
    X = zeros(N, 2);
    X(J_overlap,:) = zeros(length(J_overlap),2);
    for k = 1:length(J_overlap)
        if isempty(J_overlap)
            break;
        elseif a*sum(Tij.*X(:,1).*cp(:,1))>N_D*T && a*sum(Tij.*X(:,2).*cp(:,2))>N_D*T
            break;
        elseif a*sum(Tij.*X(:,1).*cp(:,1))<N_D*T && a*sum(Tij.*X(:,2).*cp(:,2))>N_D*T
            j_b = ran_ed(k);
            if a*Tij(J_overlap(j_b))*cp(J_overlap(j_b),1)+a*sum(Tij.*X(:,1).*cp(:,1))<=N_D*T
               X(J_overlap(j_b),:) = [1 0];
            else
                break;
            end
            J_overlap(j_b) = [];
        elseif a*sum(Tij.*X(:,1).*cp(:,1))>N_D*T && a*sum(Tij.*X(:,2).*cp(:,2))<N_D*T
            j_b = ran_ed(k);
            if a*Tij(J_overlap(j_b))*cp(J_overlap(j_b),2)+a*sum(Tij.*X(:,2).*cp(:,2))<=N_D*T
                X(J_overlap(j_b),:) = [0 1];
            else
            end
            J_overlap(j_b) = [];
        else
            j_b = ran_ed(k);
            if ran_gw(k)==1 && a*Tij(J_overlap(j_b))*cp(J_overlap(j_b),1)+a*sum(Tij.*X(:,1).*cp(:,1))<=N_D*T
                X(J_overlap(j_b),:) = [1 0];
            elseif ran_gw(k)==2 && a*Tij(J_overlap(j_b))*cp(J_overlap(j_b),2)+a*sum(Tij.*X(:,2).*cp(:,2))<=N_D*T
                X(J_overlap(j_b),:) = [0 1];
            else
                break;
            end
            J_overlap(j_b) = [];
        end
    end
    n = k;
    obj = a*sum(sum(X.*cp));
end