function s = coordinate_E(N_E, N_G, S, R)
    s = zeros(sum(N_E), 2);
    for i = 1:N_G
        if i==1
            n = 0;
        else
            n = n + N_E(i-1);
        end
        for j = 1:N_E(i)
            %r = R*rand(1, 1);
            r = R*R*rand(1, 1);
            sita = 2*pi*rand(1, 1);
            %s(n+j,:) = S(i,:) + [r*cos(sita) r*sin(sita)];
            s(n+j,:) = S(i,:) + [sqrt(r)*cos(sita) sqrt(r)*sin(sita)];
        end
    end
end