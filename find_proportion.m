function [p1, p2, p3, p4, p5, p6] = find_proportion(g1, g2, g3, g4, g5, g6)
    if ~isempty(g1)
        p1 = 1/63;
        p2 = 2/63;
        p3 = 4/63;
        p4 = 8/63;
        p5 = 16/63;
        p6 = 32/63;
    elseif ~isempty(g2)
        p1 = 0;
        p2 = 1/31;
        p3 = 2/31;
        p4 = 4/31;
        p5 = 8/31;
        p6 = 16/31;
    elseif ~isempty(g3)
        p1 = 0;
        p2 = 0;
        p3 = 1/15;
        p4 = 2/15;
        p5 = 4/15;
        p6 = 8/15;
    elseif ~isempty(g4)
        p1 = 0;
        p2 = 0;
        p3 = 0;
        p4 = 1/7;
        p5 = 2/7;
        p6 = 4/7;
    elseif ~isempty(g5)
        p1 = 0;
        p2 = 0;
        p3 = 0;
        p4 = 0;
        p5 = 1/3;
        p6 = 2/3;
    elseif ~isempty(g6)
        p1 = 0;
        p2 = 0;
        p3 = 0;
        p4 = 0;
        p5 = 0;
        p6 = 1;
    else
        p1 = 0;
        p2 = 0;
        p3 = 0;
        p4 = 0;
        p5 = 0;
        p6 = 0;
    end
end