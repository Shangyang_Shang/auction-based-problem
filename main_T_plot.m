clear;
% Parameter
d = 14;
N = 500;
R = 10;
N_D = 8;
a = 1;
small_value = -100;
Tij_table = [0.34842 0.61491 0.61541 0.61645 1.31482 2.46579];
N_i = 1000; % Number of iteration (in auction algorithm)
epsilon = 1;
N_r = 1000; % Number of random
beta = 0.0001;
bit_rate_table = [5.47 3.13 1.76 0.98 0.54 0.29]';
% Control
% T_a = 0:0.1:1;
T_a = 0:0.1:3;
% Initialization
obj      = zeros(1, length(T_a));
obj_g    = zeros(1, length(T_a));
obj_bm1  = zeros(1, length(T_a));
obj_bm2  = zeros(1, length(T_a));
obj_ub   = zeros(1, length(T_a));
ic_avg   = zeros(1, length(T_a));
ic_g_avg = zeros(1, length(T_a));
t_auc = zeros(1, length(T_a));
t_grp = zeros(1, length(T_a));
t_cnv = zeros(1, length(T_a));
t_auc_avg = zeros(1, length(T_a));
t_grp_avg = zeros(1, length(T_a));
t_cnv_avg = zeros(1, length(T_a));
for i = 1:length(T_a)
    T = T_a(i);
    for j = 1:N_r
        str = append('D:/Distance_GW2/data_fix_pow_near/data_d_', num2str(d), '_num_', num2str(j));
        load(str)
        % Find parameters
        [r1j, r2j] = find_distance(N, s, S);
        J_overlap = find_overlap(r1j, r2j, R, N, cp);
        Tij = Tij_table(SF-6)';
        % Initial: xij = 0 for overlap area
        X = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
        X(J_overlap,:) = zeros(length(J_overlap),2);
        th1_tmp = N_D*T - a*sum(Tij.*X(:,1).*cp(:,1));
        th2_tmp = N_D*T - a*sum(Tij.*X(:,2).*cp(:,2));
        th1 = max(th1_tmp, 0);
        th2 = max(th2_tmp, 0);
        % Grouping
        g1 = J_overlap(1:2:end);
        g2 = J_overlap(2:2:end);
        % Auction algorithm (Output is an array)
        tic
        [obj_tmp, ic_tmp, X_ng_tmp] = auction_algorithm(N_i, epsilon, J_overlap, N, a, cp, small_value, Tij, th1, th2, beta);
        t_auc(i) = t_auc(i) + toc;
        t_auc_avg(i) = t_auc_avg(i) + toc/ic_tmp;
        tic
        [obj_tmp1, ic_tmp1, X_g1_tmp] = auction_algorithm(N_i, epsilon, g1, N, a, cp, small_value, Tij, 0.5*th1, 0.5*th2, beta);
        t_grp_1 = toc;
        tic
        [obj_tmp2, ic_tmp2, X_g2_tmp] = auction_algorithm(N_i, epsilon, g2, N, a, cp, small_value, Tij, 0.5*th1, 0.5*th2, beta);
        t_grp_2 = toc;
        t_grp(i) = t_grp(i) + max([t_grp_1 t_grp_2]);
        t_grp_avg(i) = t_grp_avg(i) + max([t_grp_1/ic_tmp1 t_grp_2/ic_tmp2]);
        % Find benchmarks (Output is a number)
        ran_ed = zeros(1, length(J_overlap));
        ran_gw = zeros(1,length(J_overlap));
        for k = 1:length(J_overlap)
            ran_ed(k) = randi([1 length(J_overlap)-k+1]);
            ran_gw(k) = randi([1 2]);
        end
        [obj_bm1_tmp, X_bm1] = benchmark_1(cp, J_overlap, Tij, N_D, T, N, a, ran_ed);
        tic
        [obj_bm2_tmp, X_bm2, n_bm2] = benchmark_2(cp, J_overlap, Tij, N_D, T, N, a, ran_ed, ran_gw);
        t_cnv(i) = t_cnv(i) + toc;
        t_cnv_avg(i) = t_cnv_avg(i) + toc/n_bm2;
        [obj_bm3_tmp, X_ub] = benchmark_3(N, J_overlap, cp, a);
        % Save output
        X_ng = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
        X_ng(J_overlap,:) = X_ng_tmp(J_overlap,:);
        X_g = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
        X_g(g1,:) = X_g1_tmp(g1,:);
        X_g(g2,:) = X_g2_tmp(g2,:);

        obj(i)     = obj(i) + a*sum(sum(X_ng.*cp));
        obj_g(i)   = obj_g(i) + a*sum(sum(X_g.*cp));
        obj_bm1(i) = obj_bm1(i) + a*sum(sum(X_bm1.*cp));
        obj_bm2(i) = obj_bm2(i) + a*sum(sum(X_bm2.*cp));
        obj_ub(i)  = obj_ub(i) + a*sum(sum(X_ub.*cp));
        
        ic_avg(i) = ic_avg(i) + ic_tmp;
        ic_g_avg(i) = ic_g_avg(i) + max([ic_tmp1 ic_tmp2]);
    end
end
obj = obj/N_r;
obj_g = obj_g/N_r;
obj_bm1 = obj_bm1/N_r;
obj_bm2 = obj_bm2/N_r;
obj_ub = obj_ub/N_r;

t_auc = t_auc/N_r;
t_grp = t_grp/N_r;
t_cnv = t_cnv/N_r;

t_auc_avg = t_auc_avg/N_r;
t_grp_avg = t_grp_avg/N_r;
t_cnv_avg = t_cnv_avg/N_r;

ic_avg = ic_avg/N_r;
ic_g_avg = ic_g_avg/N_r;

% Plot objective function
figure(1)
plot(T_a, obj_ub, '-^r', 'LineWidth', 1.5); hold on
plot(T_a, obj_bm1, '-*k', 'LineWidth', 1.5); hold on
plot(T_a, obj_bm2, '--*k', 'LineWidth', 1.5); hold on
plot(T_a, obj, '-ob', 'LineWidth', 1.5); hold on
plot(T_a, obj_g, '--ob', 'LineWidth', 1.5); hold off
grid on
xlabel('Available time (sec)')
ylabel('Utility function')
legend('Upper bound', 'Conv.-NS', 'Conv.-RS', 'Prop. Auction Alg.', 'Prop. Grouped Alg.')

% Plot time complexity
figure(2)
subplot(211)
plot(T_a, t_cnv*1e3, '--*k', 'LineWidth', 1.5); hold on
plot(T_a, t_auc*1e3, '-ob', 'LineWidth', 1.5); hold on
plot(T_a, t_grp*1e3, '--ob', 'LineWidth', 1.5); hold off
axis([0 3 0 1.6])
grid on
title('(a) Total average execution time')
xlabel('Available time (sec)')
ylabel('Execution time (ms)')
legend('Conv.-RS', 'Prop. Auction Alg.', 'Prop. Grouped Alg.')
subplot(212)
plot(T_a, t_cnv_avg*1e3, '--*k', 'LineWidth', 1.5); hold on
plot(T_a, t_auc_avg*1e3, '-ob', 'LineWidth', 1.5); hold on
plot(T_a, t_grp_avg*1e3, '--ob', 'LineWidth', 1.5); hold off
axis([0 3 0 0.015])
grid on
title('(b) Execution time per iteration')
xlabel('Available time (sec)')
ylabel('Execution time (ms)')
legend('Conv.-RS', 'Prop. Auction Alg.', 'Prop. Grouped Alg.')

% % Plot number of iteration
% figure(2)
% plot(T_a, ic_avg, '--ob', 'LineWidth', 1.5); hold on
% plot(T_a, ic_g_avg, '-ob', 'LineWidth', 1.5); hold off
% grid on
% xlabel('Available time (sec)')
% ylabel('Number of iteration')
% legend('Non-grouping', 'Grouping')