clear;
N_r = 600;
d = 14:1:20;
for i = 1:length(d)
    for j = 1:N_r
        str_r = append('D:/Distance_GW2/data_14_20_eps_1/data_d_', num2str(d(i)), '_num_', num2str(j));
        load(str_r)
        str_w = append('D:/Distance_GW2/data_fix_pow/data_d_', num2str(d(i)), '_num_', num2str(j));
        save(str_w, 'S', 's', 'cp', 'SF')
    end
end
