clear
% 1: epsilon = 1
% 2: epsilon = 0.1
d = 14:1:20;
N_r = 200;
a = 0.1;
assign1_ng = zeros(N_r, length(d));
assign1_g = zeros(N_r, length(d));
obj1_ng = zeros(N_r, length(d));
obj1_g = zeros(N_r, length(d));
assign2_ng = zeros(N_r, length(d));
assign2_g = zeros(N_r, length(d));
obj2_ng = zeros(N_r, length(d));
obj2_g = zeros(N_r, length(d));
for i = 1:length(d)
    for j = 1:N_r
        str1 = append("C:\Users\Lab611\Desktop\Distance_GW2\data_14_20_uni_pow\data_d_", num2str(d(i)), '_num_', num2str(j));
        load(str1)
        assign1_ng(j,i) = sum(sum(X_ng));
        assign1_g(j,i) = sum(sum(X_g));
        obj1_ng(j,i) = a*sum(sum(X_ng));
        obj1_g(j,i) = a*sum(sum(X_g));
        str2 = append("C:\Users\Lab611\Desktop\Distance_GW2\data_14_20_eps_01\data_d_", num2str(d(i)), '_num_', num2str(j));
        load(str2)
        assign2_ng(j,i) = sum(sum(X_ng));
        assign2_g(j,i) = sum(sum(X_g));
        obj2_ng(j,i) = a*sum(sum(X_ng));
        obj2_g(j,i) = a*sum(sum(X_g));
    end
end
% disp(sum(assign1_ng)/N_r)
% disp(sum(assign1_g)/N_r)
% disp(sum(assign2_ng)/N_r)
% disp(sum(assign2_g)/N_r)

% disp(sum(assign1_ng)/N_r)
% disp(sum(assign1_g)/N_r)
% disp(sum(assign2_ng)/N_r)
% disp(sum(assign2_g)/N_r)

