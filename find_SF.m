function SF = find_SF(r1, r2, R)
%     bd = [0.4526 0.5379 0.6394 0.7599 0.8775 1.0133];
    bd = [0.3475 0.4375 0.5508 0.6934 0.8401 1.0178];
    r = min([r1 r2]);
    if r<bd(1)
        SF = 7;
    elseif r>=bd(1) && r<bd(2)
        SF = 8;
    elseif r>=bd(2) && r<bd(3)
        SF = 9;
    elseif r>=bd(3) && r<bd(4)
        SF = 10;
    elseif r>=bd(4) && r<bd(5)
        SF = 11;
    else
        SF = 12;
    end
%     u = R/6;
%     if r<u
%         SF = 7;
%     elseif r>=u && r<u*2
%         SF = 8;
%     elseif r>=u*2 && r<u*3
%         SF = 9;
%     elseif r>=u*3 && r<u*4
%         SF = 10;
%     elseif r>=u*4 && r<u*5
%         SF = 11;
%     else % r>=u*5
%         SF = 12;
%     end
end