clear;
% Parameter
N = 500;
R = 10;
N_D = 8;
a = 0.1;
small_value = -100;
Tij_table = [0.34842 0.61491 0.61541 0.61645 1.31482 2.46579];
T = 0.05;
N_i = 1000; % Number of iteration (in auction algorithm)
epsilon = 1;
N_r = 1; % Number of random
beta = 0.0001;
bit_rate_table = [5.47 3.13 1.76 0.98 0.54 0.29]';
d = 14;
j = randi([1 200]);

str = append('D:/Distance_GW2/data_fix_pow_new/data_d_', num2str(d), '_num_', num2str(j));
load(str)
% Find parameters
[r1, r2] = find_distance(N, s, S);
J_overlap = find_overlap(r1, r2, R, N, cp);
Tij = Tij_table(SF-6)';
% Initial: xij = 0 for overlap area
X = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
X(J_overlap,:) = zeros(length(J_overlap),2);
th1_tmp = N_D*T - a*sum(Tij.*X(:,1).*cp(:,1));
th2_tmp = N_D*T - a*sum(Tij.*X(:,2).*cp(:,2));
th1 = max(th1_tmp, 0);
th2 = max(th2_tmp, 0);
% Grouping
g1 = J_overlap(1:2:end);
g2 = J_overlap(2:2:end);
% Auction algorithm (Output is an array)
[obj_tmp, ic_tmp, X_ng_tmp] = auction_algorithm(N_i, epsilon, J_overlap, N, a, cp, small_value, Tij, th1, th2, beta);
[obj_tmp1, ic_tmp1, X_g1_tmp] = auction_algorithm(N_i, epsilon, g1, N, a, cp, small_value, Tij, 0.5*th1, 0.5*th2, beta);
[obj_tmp2, ic_tmp2, X_g2_tmp] = auction_algorithm(N_i, epsilon, g2, N, a, cp, small_value, Tij, 0.5*th1, 0.5*th2, beta);
% Find benchmarks (Output is a number)
[obj_bm1_tmp, X_bm1] = benchmark_1(cp, J_overlap, Tij, N_D, T, N, a);
[obj_bm2_tmp, X_bm2] = benchmark_2(cp, J_overlap, Tij, N_D, T, N, a);
[obj_bm3_tmp, X_ub] = benchmark_3(N, J_overlap, cp, a);
% Sum metrics
ic_g_tmp = max([ic_tmp1 ic_tmp2]);
% Save output
X_ng = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
X_ng(J_overlap,:) = X_ng_tmp(J_overlap,:);
X_g = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
X_g(g1,:) = X_g1_tmp(g1,:);
X_g(g2,:) = X_g2_tmp(g2,:);

obj = a*sum(sum(X_ng.*cp));
obj_g = a*sum(sum(X_g.*cp));
obj_bm1 = a*sum(sum(X_bm1.*cp));
obj_bm2 = a*sum(sum(X_bm2.*cp));
obj_ub = a*sum(sum(X_ub.*cp));
br = bit_rate_table(SF - 6);
sum_rate = a*sum(sum(X_ng.*cp.*[br br]));
sum_rate_g = a*sum(sum(X_g.*cp.*[br br]));
sum_rate_bm1 = a*sum(sum(X_bm1.*cp.*[br br]));
sum_rate_bm2 = a*sum(sum(X_bm2.*cp.*[br br]));
sum_rate_ub = a*sum(sum(X_ub.*cp.*[br br]));
ic_avg = ic_tmp;
ic_g_avg = ic_g_tmp;

disp([sum(sum(X_ng(J_overlap,:))) sum(sum(X_g(J_overlap,:))) sum(sum(X_bm1(J_overlap,:))) sum(sum(X_bm2(J_overlap,:))) sum(sum(X_ub(J_overlap,:)))])
disp([obj obj_g obj_bm1 obj_bm2 obj_ub])
% disp([ic_avg ic_g_avg])
% disp(cp(J_overlap,:).*(X_bm1(J_overlap,:)-X_ng(J_overlap,:)))

% t = 0:0.001:2*pi;
% cov = [R*cos(t)' R*sin(t)'];
% cov_1 = zeros(length(t), 2);
% cov_2 = zeros(length(t), 2);
% for i = 1:length(t)
%     cov_1(i,:) = cov(i,:) + S(1,:);
%     cov_2(i,:) = cov(i,:) + S(2,:);
% end
% s1 = zeros(N,2);
% s2 = zeros(N,2);
% idx1 = 0;
% idx2 = 0;
% s1_bm1 = zeros(N,2);
% s2_bm1 = zeros(N,2);
% idx1_bm1 = 0;
% idx2_bm1 = 0;
% s1_bm2 = zeros(N,2);
% s2_bm2 = zeros(N,2);
% idx1_bm2 = 0;
% idx2_bm2 = 0;
% for i = 1:N
%     if X_ng(i,1)==1
%         idx1 = idx1 + 1;
%         s1(idx1,:) = s(i,:);
%     end
%     if X_ng(i,2)==1
%         idx2 = idx2 + 1;
%         s2(idx2,:) = s(i,:);
%     end
%     if X_bm1(i,1)==1
%         idx1_bm1 = idx1_bm1 + 1;
%         s1_bm1(idx1_bm1,:) = s(i,:);
%     end
%     if X_bm1(i,2)==1
%         idx2_bm1 = idx2_bm1 + 1;
%         s2_bm1(idx2_bm1,:) = s(i,:);
%     end
%     if X_bm2(i,1)==1
%         idx1_bm2 = idx1_bm2 + 1;
%         s1_bm2(idx1_bm2,:) = s(i,:);
%     end
%     if X_bm2(i,2)==1
%         idx2_bm2 = idx2_bm2 + 1;
%         s2_bm2(idx2_bm2,:) = s(i,:);
%     end
% end
% s1 = s1(1:idx1,:);
% s2 = s2(1:idx2,:);
% s1_bm1 = s1_bm1(1:idx1_bm1,:);
% s2_bm1 = s2_bm1(1:idx2_bm1,:);
% s1_bm2 = s1_bm2(1:idx1_bm2,:);
% s2_bm2 = s2_bm2(1:idx2_bm2,:);
% figure(1)
% scatter(s1(:,1), s1(:,2), 'ob', 'LineWidth', 1.5); hold on
% scatter(s2(:,1), s2(:,2), 'og', 'LineWidth', 1.5); hold on
% scatter(cov_1(:,1), cov_1(:,2), '.r', 'LineWidth', 1.5); hold on
% scatter(cov_2(:,1), cov_2(:,2), '.r', 'LineWidth', 1.5); hold on
% scatter(S(:,1), S(:,2), '^r', 'LineWidth', 1.5); hold off
% axis equal
% grid on
% title(['Obj of pro. alg. = ' num2str(obj)])
% figure(2)
% scatter(s1_bm1(:,1), s1_bm1(:,2), 'ob', 'LineWidth', 1.5); hold on
% scatter(s2_bm1(:,1), s2_bm1(:,2), 'og', 'LineWidth', 1.5); hold on
% scatter(cov_1(:,1), cov_1(:,2), '.r', 'LineWidth', 1.5); hold on
% scatter(cov_2(:,1), cov_2(:,2), '.r', 'LineWidth', 1.5); hold on
% scatter(S(:,1), S(:,2), '^r', 'LineWidth', 1.5); hold off
% axis equal
% grid on
% title(['Obj of bm1 = ' num2str(obj_bm1)])
% figure(3)
% scatter(s1_bm2(:,1), s1_bm2(:,2), 'ob', 'LineWidth', 1.5); hold on
% scatter(s2_bm2(:,1), s2_bm2(:,2), 'og', 'LineWidth', 1.5); hold on
% scatter(cov_1(:,1), cov_1(:,2), '.r', 'LineWidth', 1.5); hold on
% scatter(cov_2(:,1), cov_2(:,2), '.r', 'LineWidth', 1.5); hold on
% scatter(S(:,1), S(:,2), '^r', 'LineWidth', 1.5); hold off
% axis equal
% grid on
% title(['Obj of bm2 = ' num2str(obj_bm1)])
