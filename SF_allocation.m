function SF = SF_allocation(r,R)
    u = R/6;
    if r<u
        SF = 7;
    elseif r>=u && r<u*2
        SF = 8;
    elseif r>=u*2 && r<u*3
        SF = 9;
    elseif r>=u*3 && r<u*4
        SF = 10;
    elseif r>=u*4 && r<u*5
        SF = 11;
    else % r>=u*5
        SF = 12;
    end
end