# Auction Based Problem
In this section, information of the project is described, including file list and generation of data
## Run the code:
* Step 1. Create folder (to store generated data)
    * e.g.
        * opt_prob_data/fix_pow_near/d_10.mat
        * opt_prob_data/fix_pow_near/d_10/1.mat
        * opt_prob_data/fix_pow_near/d_10/2.mat
        * opt_prob_data/fix_pow_near/d_10/3.mat
        * d_10.mat:   d = 1km, 5000 end-devices (generate info.)
        * d_10/1.mat: d = 1km, 500 end-devices (sample)
* Step 2. Run gen_info_and_sample.m
    * Time requirement:
        * 930 sec is required to generate information of 5000 end-devices
        * 0.9 sec to sample 500 end-devices
        * e.g. d = 1:0.1:2, 1000 samples
            * Generate information: 11*930 -> 2.8 (hr.)
            * Sample: 11*1000*0.9 -> 2.75 (hr.)
* Step 3. Run main_AvailTime_UtFunc.m or main_Dis_UtFunc.m

## File list:
* gen_info_and_sample.m
* main_AvailTime_UtFunc.m
* main_Dis_UtFunc.m

