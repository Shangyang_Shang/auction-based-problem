clear;
load('D:/opt_prob_data/fix_pow_near/d_14.mat');
disp(S)
s_sel = zeros(500, 2);
cp_sel = zeros(500, 2);
SF_sel = zeros(500, 1);
sel_set1 = randsample(1:2500, 250);
sel_set2 = randsample(2501:5000, 250);
s_sel(1:250,:) = s(sel_set1,:);
cp_sel(1:250,:) = cp(sel_set1,:);
SF_sel(1:250) = SF(sel_set1);
s_sel(251:500,:) = s(sel_set2,:);
cp_sel(251:500,:) = cp(sel_set2,:);
SF_sel(251:500) = SF(sel_set2);

str = append('D:/opt_prob_data/fix_pow_near_original/d_14_num_', num2str(randi(100)));
load(str)
disp(S)



figure(1)
subplot(211)
scatter(s_sel(1:250,1), s_sel(1:250,2), 'ob'); hold on
scatter(s_sel(251:500,1), s_sel(251:500,2), 'oc'); hold on
scatter(S(:,1), S(:,2), '^r', 'LineWidth', 1.5); hold off
grid on
axis equal
subplot(212)
scatter(s(1:250,1), s(1:250,2), 'ob'); hold on
scatter(s(251:500,1), s(251:500,2), 'oc'); hold on
scatter(S(:,1), S(:,2), '^r', 'LineWidth', 1.5); hold off
grid on
axis equal

figure(2)
subplot(211)
histogram(SF_sel, 'LineWidth', 1.5, 'EdgeColor', 'b', 'FaceColor', 'none')
axis([6.5 12.5 0 150])
grid on
xlabel('spreading factor')
ylabel('counts')
subplot(212)
histogram(SF, 'LineWidth', 1.5, 'EdgeColor', 'b', 'FaceColor', 'none')
axis([6.5 12.5 0 150])
grid on
xlabel('spreading factor')
ylabel('counts')

disp([sum(sum(cp_sel))/500 sum(sum(cp))/500])
figure(3)
subplot(211)
histogram(cp_sel, 'LineWidth', 1.5, 'EdgeColor', 'b', 'FaceColor', 'none')
grid on
subplot(212)
histogram(cp, 'LineWidth', 1.5, 'EdgeColor', 'b', 'FaceColor', 'none')
grid on
