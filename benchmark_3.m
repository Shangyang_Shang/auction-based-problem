function [obj, X] = benchmark_3(N, J_overlap, cp, a)
    % Upper bound
    X = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
    X(J_overlap,:) = ones(length(J_overlap),2);
    for i = 1:N
        if X(i,1)==1 && X(i,2)==1
            if cp(i,1)>cp(i,2)
                X(i,:) = [1 0];
            else
                X(i,:) = [0 1];
            end
        end
    end
    obj = a*sum(sum(X.*cp));
end