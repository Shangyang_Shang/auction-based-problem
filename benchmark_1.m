function [obj, X, n] = benchmark_1(cp, J_overlap, Tij, N_D, T, N, a, ran_ed)
    % Initial: xij = 0 for overlap area
    %X = [ones(N/2,1) zeros(N/2,1);zeros(N/2,1) ones(N/2,1)];
    X = zeros(N, 2);
    X(J_overlap,:) = zeros(length(J_overlap),2);
    for i = 1:length(J_overlap)
        if isempty(J_overlap)
            break;
        elseif a*sum(Tij.*X(:,1).*cp(:,1))>N_D*T && a*sum(Tij.*X(:,2).*cp(:,2))>N_D*T
            break;
        elseif a*sum(Tij.*X(:,1).*cp(:,1))<N_D*T && a*sum(Tij.*X(:,2).*cp(:,2))>N_D*T
%             j_b = randi([1 length(J_overlap)]);
            j_b = ran_ed(i);
            if a*Tij(J_overlap(j_b))*cp(J_overlap(j_b),1)+a*sum(Tij.*X(:,1).*cp(:,1))<=N_D*T
                X(J_overlap(j_b),:) = [1 0];
            end
            J_overlap(j_b) = [];
        elseif a*sum(Tij.*X(:,1).*cp(:,1))>N_D*T && a*sum(Tij.*X(:,2).*cp(:,2))<N_D*T
%             j_b = randi([1 length(J_overlap)]);
            j_b = ran_ed(i);
            if a*Tij(J_overlap(j_b))*cp(J_overlap(j_b),2)+a*sum(Tij.*X(:,2).*cp(:,2))<=N_D*T
                X(J_overlap(j_b),:) = [0 1];
            end
            J_overlap(j_b) = [];
        else
%             j_b = randi([1 length(J_overlap)]);
            j_b = ran_ed(i);
            v1 = a*Tij(J_overlap(j_b))*cp(J_overlap(j_b),1)+a*sum(Tij.*X(:,1).*cp(:,1));
            v2 = a*Tij(J_overlap(j_b))*cp(J_overlap(j_b),2)+a*sum(Tij.*X(:,2).*cp(:,2));
            if v1<=N_D*T && v2<=N_D*T
                X(J_overlap(j_b),:) = [1 1];
            end
            J_overlap(j_b) = [];
        end
    end
    n = i;
    % Choose higher capture probability to calculate objective function
    for i = 1:N
        if X(i,1)==1 && X(i,2)==1
            if cp(i,1)>cp(i,2)
                X(i,:) = [1 0];
            else
                X(i,:) = [0 1];
            end
        end
    end
    obj = a*sum(sum(X.*cp));
end